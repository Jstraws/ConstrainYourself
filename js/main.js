var count = 0;
var buttons;
var reg = {};
var pin = "1234";

var mainState = {
    preload: function () {
        game.load.spritesheet('zero', 'assets/img/0.png', 156, 159);
        game.load.spritesheet('one', 'assets/img/1.png', 156, 159);
        game.load.spritesheet('two', 'assets/img/2.png', 156, 159);
        game.load.spritesheet('three', 'assets/img/3.png', 156, 159);
        game.load.spritesheet('four', 'assets/img/4.png', 156, 159);
        game.load.spritesheet('five', 'assets/img/5.png', 156, 159);
        game.load.spritesheet('six', 'assets/img/6.png', 156, 159);
        game.load.spritesheet('seven', 'assets/img/7.png', 156, 159);
        game.load.spritesheet('eight', 'assets/img/8.png', 156, 159);
        game.load.spritesheet('nine', 'assets/img/9.png', 156, 159);
        game.load.spritesheet('asterisk', 'assets/img/asterisk.png', 156, 159);
        game.load.spritesheet('pound', 'assets/img/pound.png', 156, 159);
        game.load.spritesheet('clear', 'assets/img/c.png', 156, 159);
        game.load.image('screen', 'assets/img/screen.png');
    },

    create: function () {
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;
        game.scale.refresh();

        var newHeight = height + 125;
        var btnSize = 75;

        game.stage.backgroundColor = '363636';

        //Creating modals for submission popup
        reg.modal = new gameModal(game);
        reg.modal.createModal({
            type:"login",
            includeBackground: true,
            modalCloseOnInput: true,
            itemsArr:[
                {
                    type: "text",
                    content: "PIN Entered Successfully!\nLogging In...\n\nClick to Close",
                    fontFamily: "Arial",
                    fontSize: 38,
                    color: "0xffffff",
                    offsetY: -50,
                    stroke: "0x000000",
                    strokeThickness: 5
                }
            ]
        });

        reg.modal.createModal({
            type:"newPin",
            includeBackground: true,
            modalCloseOnInput: true,
            itemsArr:[
                {
                    type: "text",
                    content: "PIN Changed Successfully!\nDon't Forget It!",
                    fontFamily: "Arial",
                    fontSize: 38,
                    color: "0xffffff",
                    offsetY: -50,
                    stroke: "0x000000",
                    strokeThickness: 5
                }
            ]
        });

        reg.modal.createModal({
            type:"wrongPin",
            includeBackground: true,
            modalCloseOnInput: true,
            itemsArr:[
                {
                    type: "text",
                    content: "PIN Entered Incorrectly!\nCalling NSA & FBI...\n\nClick to Close",
                    fontFamily: "Arial",
                    fontSize: 38,
                    color: "0xffffff",
                    offsetY: -50,
                    stroke: "0x000000",
                    strokeThickness: 5
                }
            ]
        });

        this.screen = game.add.sprite(width/2, (newHeight/2)-btnSize*2.25, 'screen');
        this.screen.anchor.setTo(0.5, 0.5);

        var optionStyle = { font: '30pt Arial', fill: 'White', align: 'center', stroke: 'rgba(0,0,0,0)', strokeThickness: 4};
        this.screenText = game.add.text(this.screen.x, this.screen.y, "",optionStyle);
        this.screenText.anchor.setTo(0.5, 0.5);

        this.instruction = game.add.text(this.screenText.x, 50, "Default PIN is 1234. Enter a new PIN and press the * to change.\nEnter current PIN and press # to login.", optionStyle);
        this.instruction.fontSize = 18;
        this.instruction.anchor.setTo(0.5, 0.5);

        buttons = game.add.group();

        this.clear = game.add.button(this.screen.x+(this.screen.width/2)+30, this.screen.y, 'clear', this.clear, this, 0, 0, 1);
        this.clear.anchor.setTo(0.5, 0.5);
        this.clear.input.useHandCursor = true;
        this.clear.scale.setTo(.25);
        buttons.add(this.clear);

        this.one = game.add.button((width/2)-btnSize, (newHeight/2)-btnSize, 'one', this.numPress, this, 0, 0, 1);
        this.one.anchor.setTo(0.5, 0.5);
        this.one.input.useHandCursor = true;
        this.one.scale.setTo(.45);
        this.one.name = "1";
        buttons.add(this.one);

        this.two = game.add.button(width/2, (newHeight/2)-btnSize, 'two', this.numPress, this, 0, 0, 1);
        this.two.anchor.setTo(0.5, 0.5);
        this.two.input.useHandCursor = true;
        this.two.scale.setTo(.45);
        this.two.name = "2";
        buttons.add(this.two);

        this.three = game.add.button((width/2)+btnSize, (newHeight/2)-btnSize, 'three', this.numPress, this, 0, 0, 1);
        this.three.anchor.setTo(0.5, 0.5);
        this.three.input.useHandCursor = true;
        this.three.scale.setTo(.45);
        this.three.name = "3";
        buttons.add(this.three);

        this.four = game.add.button((width/2)-btnSize, newHeight/2, 'four', this.numPress, this, 0, 0, 1);
        this.four.anchor.setTo(0.5, 0.5);
        this.four.input.useHandCursor = true;
        this.four.scale.setTo(.45);
        this.four.name = "4";
        buttons.add(this.four);

        this.five = game.add.button(width/2, newHeight/2, 'five', this.numPress, this, 0, 0, 1);
        this.five.anchor.setTo(0.5, 0.5);
        this.five.input.useHandCursor = true;
        this.five.scale.setTo(.45);
        this.five.name = "5";
        buttons.add(this.five);

        this.six = game.add.button((width/2)+btnSize, newHeight/2, 'six', this.numPress, this, 0, 0, 1);
        this.six.anchor.setTo(0.5, 0.5);
        this.six.input.useHandCursor = true;
        this.six.scale.setTo(.45);
        this.six.name = "6";
        buttons.add(this.six);

        this.seven = game.add.button((width/2)-btnSize, (newHeight/2)+btnSize, 'seven', this.numPress, this, 0, 0, 1);
        this.seven.anchor.setTo(0.5, 0.5);
        this.seven.input.useHandCursor = true;
        this.seven.scale.setTo(.45);
        this.seven.name = "7";
        buttons.add(this.seven);

        this.eight = game.add.button(width/2, (newHeight/2)+btnSize, 'eight', this.numPress, this, 0, 0, 1);
        this.eight.anchor.setTo(0.5, 0.5);
        this.eight.input.useHandCursor = true;
        this.eight.scale.setTo(.45);
        this.eight.name = "8";
        buttons.add(this.eight);

        this.nine = game.add.button((width/2)+btnSize, (newHeight/2)+btnSize, 'nine', this.numPress, this, 0, 0, 1);
        this.nine.anchor.setTo(0.5, 0.5);
        this.nine.input.useHandCursor = true;
        this.nine.scale.setTo(.45);
        this.nine.name = "9";
        buttons.add(this.nine);
        
        this.asterisk = game.add.button((width/2)-btnSize, (newHeight/2)+btnSize*2, 'asterisk', this.asteriskPress, this, 0, 0, 1);
        this.asterisk.anchor.setTo(0.5, 0.5);
        this.asterisk.input.useHandCursor = true;
        this.asterisk.scale.setTo(.45);
        this.asterisk.name = "*";
        buttons.add(this.asterisk);

        this.zero = game.add.button(width/2, (newHeight/2)+btnSize*2, 'zero', this.numPress, this, 0, 0, 1);
        this.zero.anchor.setTo(0.5, 0.5);
        this.zero.input.useHandCursor = true;
        this.zero.scale.setTo(.45);
        this.zero.name = "0";
        buttons.add(this.zero);

        this.pound = game.add.button((width/2)+btnSize, (newHeight/2)+btnSize*2, 'pound', this.poundPress, this, 0, 0, 1);
        this.pound.anchor.setTo(0.5, 0.5);
        this.pound.input.useHandCursor = true;
        this.pound.scale.setTo(.45);
        this.pound.name = "#";
        buttons.add(this.pound);
    },

    update: function () {

    },

    numPress: function(item) {
        if(count < 4) {
            this.screenText.text += item.name;
            count += 1;
        } else {
            this.screenText.text = "ERROR";
            game.input.enabled = false;
            game.time.events.add(Phaser.Timer.SECOND*2, function(){
                this.screenText.text = "";
                count = 0;
                game.input.enabled = true;
            }, this);

        }
    },

    poundPress: function () {
        if(count === 4 && this.screenText.text === pin) {
            reg.modal.showModal("login");
            this.screenText.text = "";
            count = 0;
        } else if (count === 4 && this.screenText.text !== pin){
            reg.modal.showModal("wrongPin");
            this.screenText.text = "";
            count = 0;
        } else {
            this.screenText.text = "ERROR";
            game.input.enabled = false;
            game.time.events.add(Phaser.Timer.SECOND*2, function(){
                this.screenText.text = "";
                count = 0;
                game.input.enabled = true;
            }, this);
        }
    },

    asteriskPress: function() {
        if(count === 4) {
            pin = this.screenText.text;
            reg.modal.showModal("newPin");
            this.screenText.text = "";
            count = 0;
        } else {
            this.screenText.text = "ERROR";
            game.input.enabled = false;
            game.time.events.add(Phaser.Timer.SECOND*2, function(){
                this.screenText.text = "";
                count = 0;
                game.input.enabled = true;
            }, this);
        }
    },

    clear: function () {
        this.screenText.text = "";
        count = 0;
    }
};

var width = 600;
var height = 600;

// Initialize Phaser
var game = new Phaser.Game(width, height, Phaser.CANVAS);

// Add states
game.state.add('main', mainState);

// Start the state
game.state.start('main');
